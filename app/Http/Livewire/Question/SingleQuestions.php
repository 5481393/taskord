<?php

namespace App\Http\Livewire\Question;

use Livewire\Component;

class SingleQuestions extends Component
{
    public function render()
    {
        return view('livewire.question.single-questions');
    }
}
