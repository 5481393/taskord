<?php

namespace App\Http\Livewire\Answer;

use Livewire\Component;

class SingleAnswers extends Component
{
    public function render()
    {
        return view('livewire.answer.single-answers');
    }
}
