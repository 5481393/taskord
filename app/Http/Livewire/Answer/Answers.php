<?php

namespace App\Http\Livewire\Answer;

use Livewire\Component;

class Answers extends Component
{
    public function render()
    {
        return view('livewire.answer.answers');
    }
}
